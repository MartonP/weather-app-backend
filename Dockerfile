FROM node:10-alpine

ARG owm_api_key

RUN mkdir -p /home/node/weatherapp/node_modules && chown -R node:node /home/node/weatherapp
WORKDIR /home/node/weatherapp

COPY package*.json ./
USER node
RUN npm install
RUN npm i g nodemon
COPY --chown=node:node . .

ENV OWM_API_KEY=$owm_api_key

EXPOSE 5001

CMD ["npm", "start"]