exports.formatWeather = (data) => {
  const {
    clouds: {
      all: clouds_percent
    },
    main: {
      temp,
      temp_min,
      temp_max,
      humidity,
      pressure
    },
    wind: {
      speed: wind_speed
    },
    sys: {
      sunrise,
      sunset
    },
    weather
  } = data
  const type = (weather.length === 0) ? null : weather[0].main
  const type_description = (weather.length === 0) ? null : weather[0].description
  return {
    type,
    type_description,
    sunrise: new Date(sunrise * 1000),
    sunset: new Date(sunset * 1000),
    temp,
    temp_min,
    temp_max,
    pressure,
    humidity,
    clouds_percent,
    wind_speed
  }
}

exports.formatCities = (cities) => {
  if (cities.length === 0) return []
  return cities.map((city) => {
    return {
      id: city.id,
      name: city.name
    }
  })
}

exports.formatCity = (city) => {
  const { id, name, coord: { lat, lon: lng } } = city
  return {
    id,
    name,
    lat,
    lng
  }
}