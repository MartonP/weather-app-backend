const express = require('express')
const router = express.Router()

const cities = require('./cities')

router.use('/cities', cities)

module.exports = router
