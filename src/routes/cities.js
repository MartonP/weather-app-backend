const express = require('express')
const router = express.Router()

const CityController = require('../controllers/CityController')

router.get('/', CityController.getCities)
router.get('/:cityId', CityController.getCity)
router.get('/:cityId/weather', CityController.getWeather)

module.exports = router
