exports.convertKmToDegrees = ({ lat, long, distance }) => {
  const latDegree = 1 / 111 * distance
  const longDegree = 1 / 111 * Math.cos(( lat * (Math.PI/180))) * distance
  return { latDegree, longDegree }
}