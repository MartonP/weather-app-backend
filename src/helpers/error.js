class ApplicationError extends Error {
  constructor (message, status) {
    super(message)
    this.code = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
    this.status = status || 500
  }
}

class NotFoundError extends ApplicationError {
  constructor (message) {
    super(message || 'not found', 404)
  }
}

class BadRequestError extends ApplicationError {
  constructor (message) {
    super(message || 'bad request', 400)
  }
}

const handleErrors = (err, req, res, next) => {
  if (!err) {
    next()
    return
  }
  if (err instanceof ApplicationError) {
    res.status(err.status).json({
      code: err.code,
      message: err.message,
    })
    return
  }
  res.status(500).json({ message: err.toString(), code: 'Internal server error' })
}

module.exports = { handleErrors, NotFoundError, BadRequestError }
