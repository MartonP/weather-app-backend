const axios = require('axios')

const weatherFormatter = require('../formatters/weatherFormatter')
const { NotFoundError, BadRequestError } = require('../helpers/error')
const convertKmToDegrees = require('../helpers/convertKmToDegrees').convertKmToDegrees
const citiesJSON = require('../city.list.json')


exports.getCities = async ({ query }, res, next) => {
  try {
    const { lat, lng } = query
    if (!lat || !lng) throw new BadRequestError('lat/lng required')
    if (isNaN(parseFloat(lat)) || isNaN(parseFloat(lng))) throw new BadRequestError('lat/lng format invalid')
    const { latDegree, longDegree } = convertKmToDegrees({ lat, long: lng, distance: 10 })
    const latF = parseFloat(lat)
    const lngF = parseFloat(lng)
    const cities = citiesJSON.filter((city) => 
      (Math.pow((city.coord.lat - latF), 2)/Math.pow(latDegree, 2) + Math.pow((city.coord.lon - lngF), 2)/Math.pow(longDegree, 2)) <= 1
    )
    if (!cities || cities.length === 0) throw new NotFoundError()
    res.json(weatherFormatter.formatCities(cities))
  } catch (err) {
    console.error(err)
    next(err)
  }
}

exports.getCity = async ({ params }, res, next) => {
  try {
    const { cityId } = params
    const data = citiesJSON
    const cities = data.filter((city) => city.id == cityId)
    if (!cities || cities.length === 0) throw new NotFoundError()
    res.json(weatherFormatter.formatCity(cities[0]))
  } catch (err) {
    console.error(err)
    next(err)
  }
}

exports.getWeather = async ({ params }, res, next) => {
  try {
    const { cityId } = params
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?id=${cityId}&APPID=${process.env.OWM_API_KEY}`)
    if (!response) throw new NotFoundError()
    res.json(weatherFormatter.formatWeather(response.data))
  } catch (err) {
    console.error(err)
    if (!err.response) next(err)
    switch (err.response.status) {
      case 400: next(new BadRequestError())
      case 404: next(new NotFoundError())
      default: next(err)
    }
  }
}

