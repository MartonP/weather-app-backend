const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../app')

chai.use(chaiHttp)
const expect = chai.expect

describe("Cities", () => {
  describe("GET /cities?lat&lng", () => {
    it("should return Budapest & districts", (done) => {
      chai.request(app)
        .get('/cities?lat=47.4983815&lng=19.0404707')
        .end((err, res) => {
          expect(res).to.have.status(200)
          expect(err).to.be.null
          expect(res.body).to.be.deep.equal([
              {
                  "id": 7284824,
                  "name": "Budapest XI. keruelet"
              },
              {
                  "id": 7284825,
                  "name": "Budapest IX. keruelet"
              },
              {
                  "id": 7284826,
                  "name": "Budapest VIII. keruelet"
              },
              {
                  "id": 7284827,
                  "name": "Budapest VII. keruelet"
              },
              {
                  "id": 7284828,
                  "name": "Budapest VI. keruelet"
              },
              {
                  "id": 7284830,
                  "name": "Budapest XIII. keruelet"
              },
              {
                  "id": 7284838,
                  "name": "Budapest XXII. keruelet"
              },
              {
                  "id": 7284839,
                  "name": "Budapest XXI. keruelet"
              },
              {
                  "id": 7284842,
                  "name": "Budapest III. keruelet"
              },
              {
                  "id": 7284843,
                  "name": "Budapest II. keruelet"
              },
              {
                  "id": 7284844,
                  "name": "Budapest I. keruelet"
              },
              {
                  "id": 7284823,
                  "name": "Budapest XII. keruelet"
              },
              {
                  "id": 3054638,
                  "name": "Budapest főváros"
              },
              {
                  "id": 3054643,
                  "name": "Budapest"
              },
              {
                  "id": 3046446,
                  "name": "Pest"
              }
            ])
          done()
        })
    })
    it("should throw 400", (done) => {
      chai.request(app)
        .get('/cities?lat=&lng=')
        .end((err, res) => {
          expect(res).to.have.status(400)
          done()
        })
    })
    it("should throw 400", (done) => {
      chai.request(app)
        .get('/cities?lat=text&lng=text')
        .end((err, res) => {
          expect(res).to.have.status(400)
          done()
        })
    })
  })
  describe("GET /cities/:cityId", () => {
    it("Should return Mannheim's data.", (done) => {
      chai.request(app)
        .get('/cities/2873891')
        .end((err, res) => {
          expect(res).to.have.status(200)
          expect(err).to.be.null
          expect(res.body).to.deep.equal({
            "id": 2873891,
            "name": "Mannheim",
            "lat": 49.488331,
            "lng": 8.46472
          })
          done()
        })
    })
    it("should throw 404", (done) => {
      chai.request(app)
        .get('/cities/99999999')
        .end((err, res) => {
          expect(res).to.have.status(404)
          done()
        })
    })
  })
})

describe("Weather", () => {
  describe("GET /cities/:cityId/weather", () => {
    it("should return Mannheim's weather", (done) => {
      chai.request(app)
        .get('/cities/2873891/weather')
        .end((err, res) => {
          expect(res).to.have.status(200)
          expect(err).to.be.null
          expect(res.body).to.not.be.null
          expect(res.body).to.have.all.keys('type', 'type_description', 'sunrise', 'sunset', 'temp', 'temp_min', 'temp_max', 'pressure', 'humidity', 'clouds_percent', 'wind_speed')
          expect(res.body.type).to.satisfy((s) => {
            return s === null || typeof s == 'string'
          })
          expect(res.body.type_description).to.satisfy((s) => {
            return s === null || typeof s == 'string'
          })
          expect(res.body.sunrise).to.be.a('string')
          expect(res.body.sunset).to.be.a('string')
          expect(res.body.temp).to.be.be.a('number')
          expect(res.body.temp_min).to.be.a('number')
          expect(res.body.temp_max).to.be.a('number')
          expect(res.body.pressure).to.be.a('number')
          expect(res.body.humidity).to.be.a('number')
          expect(res.body.clouds_percent).to.be.a('number')
          expect(res.body.wind_speed).to.be.a('number')
          done()
        })
    })
    it("should throw 400", (done) => {
      chai.request(app)
        .get('/cities/text/weather')
        .end((err, res) => {
          expect(res).to.have.status(400)
          done()
        })
    })
    it("should throw 404", (done) => {
      chai.request(app)
        .get('/cities/99999999/weather')
        .end((err, res) => {
          expect(res).to.have.status(404)
          done()
        })
    })
  })
})