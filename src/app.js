const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const { handleErrors } = require('./helpers/error')
const api = require('./routes/api');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', api);
app.use(handleErrors)

const port = process.env.port || 5001

app.listen(port, (err) => {
  if (!err) {
    console.debug(`Server is listening on port ${port}`)
  } else {
    console.error(err)
  }
})

module.exports = app;
